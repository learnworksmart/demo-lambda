# Deploying AWS Lambda function using GitLab CI/CD
1. Create a new IAM user, **demo-lambda**, by running `cd prerequisites && terraform init && terraform apply --auto-approve`.  
1. Take note of the new IAM user programmatic access, by running the following commands:
    * **AWS_ACCESS_KEY_ID**, `terraform state show aws_iam_access_key.demo_lambda | grep id`
    * **AWS_SECRET_ACCESS_KEY**, `cat terraform.tfstate | grep \"secret\"\:`. The `secret access key` is stored in the terraform state file. 
1. Set up your AWS credentials in your GitLab project's `CI/CD Settings` -> `Variables`. 
    ![image](/uploads/08373c375fb9b393731e41fea3b9554b/image.png)
1. Run `git push`, observe the `pipeline` and you should be able to access the deployed node application. 
    ![image](/uploads/c87dcad2849d9dbff19ac7306b71b073/image.png)

## References
1. https://docs.gitlab.com/ee/user/project/clusters/serverless/aws.html
1. Every stage you deploy to with [serverless.yml](https://www.serverless.com/framework/docs/providers/aws/guide/serverless.yml/) using the aws provider is a single AWS CloudFormation stack. This is where your AWS Lambda functions and their event configurations are defined and it's how they are deployed.
