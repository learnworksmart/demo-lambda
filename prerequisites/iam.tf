data "aws_iam_policy_document" "demo_lambda_access_rights" {
  statement {
      effect    = "Allow"
      actions   = ["cloudformation:*",
        "s3:*",
        "logs:*",
        "iam:*",
        "apigateway:*",
        "lambda:*",
      ]
      resources = ["*"]    
  }
}

resource "aws_iam_user" "demo_lambda" {
  name = var.project
  path = "/"

  tags = {
    Name    = var.project
    Project = var.project
  }
}

resource "aws_iam_access_key" "demo_lambda" {
  user = aws_iam_user.demo_lambda.name
}

resource "aws_iam_user_policy" "demo_lambda" {
  name = var.project
  user = aws_iam_user.demo_lambda.name
  policy = data.aws_iam_policy_document.demo_lambda_access_rights.json
}
